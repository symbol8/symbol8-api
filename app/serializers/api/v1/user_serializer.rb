# frozen_string_literal: true

module Api
  module V1
    # User serializer
    class UserSerializer < ActiveModel::Serializer
      attributes %i[id email username image]
    end
  end
end
