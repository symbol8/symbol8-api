# frozen_string_literal: true

# Render helper json wrapper
module Api::V1::RenderHelper
  def api_error(status: 500, errors: [])
    # TODO: Use rails logger
    puts errors.full_messages if errors.respond_to?(:full_messages)

    render json: Api::V1::ErrorSerializer.new(status, errors).as_json,
           status: status
  end

  def render_resource(resource)
    if resource.errors.empty?
      render json: resource
    else
      api_error(400, resource.errors)
    end
  end
end
