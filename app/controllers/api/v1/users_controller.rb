# frozen_string_literal: true

# User controller api endpoint
class Api::V1::UsersController < Api::V1::BaseController
  before_action :authenticate_user!

  def show
    user = User.find(params[:id])

    render_resource user
  end

  def update
    updated = current_user.update(update_permitted_params)

    if updated
      render_resource current_user
    else
      api_error status: 422, errors: [I18n.t('errors.update_user')]
    end
  end

  private

  def update_permitted_params
    params.permit %i[username email password]
  end
end
