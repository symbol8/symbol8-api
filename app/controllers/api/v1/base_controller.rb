# frozen_string_literal: true

module Api
  module V1
    # Namespace for api endpoint
    class BaseController < ApplicationController
      include ActionController::MimeResponds
      include Api::V1::RenderHelper

      respond_to :json
    end
  end
end
