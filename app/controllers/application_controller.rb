# frozen_string_literal: true

# Main controller
class ApplicationController < ActionController::API
  include ActionController::MimeResponds
  include ActionController::Serialization

  before_action :configure_permited_parameters, if: :devise_controller?

  rescue_from ActiveRecord::RecordNotFound do
    api_error status: 404, errors: 'Resource not found.'
  end

  respond_to :json

  protected

  def configure_permited_parameters
    added_attrs = %i[username email password password_confirmation]
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs
  end
end
