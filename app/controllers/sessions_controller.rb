# frozen_string_literal: true

# Overridden session controller of devise
class SessionsController < Devise::SessionsController
  include Devise::Controllers::Helpers
  include Api::V1::RenderHelper

  prepend_before_action :require_no_authentication, only: [:create]
  before_action :rewrite_param_names, only: [:create]
  respond_to :json

  def new
    api_error(status: :unauthorized, errors: I18n.t('devise.failure.unauthenticated'))
  end

  def create
    self.resource = warden.authenticate!(auth_options)
    sign_in(resource_name, resource)
    yield resource if block_given?

    # Put token inside the resource
    resource.token = current_token if resource.respond_to?(:token)

    render json: {
      user: resource.to_json
    }
  end

  private

  def rewrite_param_names
    request.params[:user] = {
      username: request.params[:user][:login],
      password: request.params[:user][:password]
    }
  end

  def current_token
    request.env['warden-jwt_auth.token']
  end

  def respond_with(resource, _opts = {})
    render json: resource
  end

  def respond_to_on_destroy
    head :no_content
  end
end
