# frozen_string_literal: true

# Custom failure controller
class FailureAppController < Devise::FailureApp
  def http_auth_body
    {
      errors: [{
        message: i18n_message
      }]
    }.to_json
  end
end
