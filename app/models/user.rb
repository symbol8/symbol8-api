# frozen_string_literal: true

# User model, without register possibility
class User < ApplicationRecord
  devise :database_authenticatable, :trackable,
         :jwt_authenticatable,
         jwt_revocation_strategy: JWTBlacklist

  attr_accessor :token

  # Used only inside session controller
  def to_json(*_args)
    json =
      {
        id: id,
        username: username,
        image: image,
        email: email
      }
    json[:token] = token if token

    json
  end
end
