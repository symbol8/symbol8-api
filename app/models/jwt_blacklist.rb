# frozen_string_literal: true

# Blacklist to revoce jwt tokens
class JWTBlacklist < ApplicationRecord
  include Devise::JWT::RevocationStrategies::Blacklist

  self.table_name = 'jwt_blacklist'
end
