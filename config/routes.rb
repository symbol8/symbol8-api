# frozen_string_literal: true

Rails.application.routes.draw do
  # Login -> /api/v1/login
  # Logout -> /api/v1/logout
  devise_for :users,
             defaults: { format: :json },
             path: 'api/v1/sessions',
             path_names: {
               sign_in: 'create',
               sign_out: 'destroy'
             },
             controllers: {
               sessions: 'sessions'
             }

  namespace :api do
    namespace :v1 do
      resources :users, only: %i[show update]
    end
  end
end
