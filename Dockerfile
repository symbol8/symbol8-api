FROM ruby:2.6.0-slim

EXPOSE 80

MAINTAINER Yeboster<yeboster@gmail.com>

# Copy the rails app inside the container image
COPY . /application

# Set the working dir (PWD)
WORKDIR /application

# Install dependencies
RUN apt-get update && apt-get install -qq -y build-essential nodejs libpq-dev --fix-missing --no-install-recommends
RUN bundle install --without development test

# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]

# Env variables
ENV RAILS_ENV production
ENV RAILS_LOG_TO_STDOUT 1
ENV RAILS_MASTER_KEY $RAILS_MASTER_KEY

# Start the server
CMD bundle exec rails server -p 80
