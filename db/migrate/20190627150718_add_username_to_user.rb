# frozen_string_literal: true

class AddUsernameToUser < ActiveRecord::Migration[5.2]
  def up
    add_column :users, :username, :string, null: false, default: ''
    add_index :users, :username, unique: true
  end

  def down
    remove_column :users, :username
    remove_index :users, name: :index_users_on_email
  end
end
